package spacegraph;

import jcog.Util;

/** spacegraph global / static parameters */
public enum UI {
	;

	public static float FPS_default = Util.intProperty("FPS", 60);

}
